run:
	docker-compose rm -f
	docker-compose build
	docker-compose up --force-recreate

tests:
	docker-compose rm -f
	docker-compose build
	docker-compose -f docker-compose.test.yml up --force-recreate --abort-on-container-exit