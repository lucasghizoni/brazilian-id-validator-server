# Brazilian ID Validator Server

This is a server rest API to validate, save, edit and delete CPF/CNPJ's. There is a frontend application consuming this API. Here is the link of the project: [brazilian-id-validator-front](https://bitbucket.org/lucasghizoni/brazilian-id-validator-front).

The app is built using NodeJS, MongoDB and Docker Compose .


## Installation and Running the Server

Supposing you have Docker and Docker Compose installed, just type:

```
$ sudo make run
```

This command will download and build everything you need to have inside docker containers, and then provide the server up at: http://localhost:3000.

## Rest API

### GET /individuals

#### Success Response
- An array of objects with all individuals on the data base.

#### - Error Response
- CODE 500: When an internal server error occurs

### POST /individuals

#### Body request
- You need to send a json just like this:

```javascript
{id: "06875446960", blacklist: false}
```

#### - Success Response
- The json object saved

#### - Error Response
- CODE 500: When an internal server error occurs
- CODE 400: When attribute ID is invalid. Body error response: { message: "Invalid ID" }.
- CODE 303: When individual is already saved on database. Body error response: { message: "Individual already exists" }.


### GET /individual/:id

#### Success Response
- Object of the requested individual

#### - Error Response
- CODE 500: When an internal server error occurs
- CODE 400: When attribute ID is invalid. Body error response: { message: "Invalid ID" }.
- CODE 404: When individual is not on database. Body error response: { message: "Individual not found" }.

### PUT /individual/:id

#### Body request
- You need to send a json just like this:

```javascript
{id: "06875446960", blacklist: false}
```

#### - Success Response
- The json object edited

#### - Error Response
- CODE 500: When an internal server error occurs
- CODE 400: When attribute ID is invalid. Body error response: { message: "Invalid ID" }.
- CODE 404: When individual is not on database. Body error response: { message: "Individual not found" }.
- CODE 303: When individual is already saved on database. Body error response: { message: "Individual already exists" }.

### DELETE /individual/:id

#### - Success Response

```javascript
{ message: 'Individual successfully deleted' }
```

#### - Error Response
- CODE 500: When an internal server error occurs
- CODE 404: When individual is not on database. Body error response: { message: "Individual not found" }.

### GET /companies

#### Success Response
- An array of objects with all companies on the data base.

#### - Error Response
- CODE 500: When an internal server error occurs

### POST /companies

#### Body request
- You need to send a json just like this:

```javascript
{id: "50314873000147", blacklist: false}
```

#### - Success Response
- The json object saved

#### - Error Response
- CODE 500: When an internal server error occurs
- CODE 400: When attribute ID is invalid. Body error response: { message: "Invalid ID" }.
- CODE 303: When company is already saved on database. Body error response: { message: "Company already exists" }.


### GET /company/:id

#### Success Response
- Object of the requested company

#### - Error Response
- CODE 500: When an internal server error occurs
- CODE 400: When attribute ID is invalid. Body error response: { message: "Invalid ID" }.
- CODE 404: When company is not on database. Body error response: { message: "Company not found" }.

### PUT /company/:id

#### Body request
- You need to send a json just like this:

```javascript
{id: "50314873000147", blacklist: false}
```

#### - Success Response
- The json object edited

#### - Error Response
- CODE 500: When an internal server error occurs
- CODE 400: When attribute ID is invalid. Body error response: { message: "Invalid ID" }.
- CODE 404: When company is not on database. Body error response: { message: "Company not found" }.
- CODE 303: When company is already saved on database. Body error response: { message: "Company already exists" }.

### DELETE /company/:id

#### - Success Response

```javascript
{ message: 'company successfully deleted' }
```

#### - Error Response
- CODE 500: When an internal server error occurs
- CODE 404: When company is not on database. Body error response: { message: "Company not found" }.


### GET /status

#### - Success Response

```javascript
{ requests: 8, uptime: '45 minutes' }
```

## Tests

### Unit

There are unit tests covering just the IdValidator.js. This file has 2 public methods, responsible for validating CPFs and CNPJs, according to the correct rules for each one.

### API

For API tests, the chai-http nodejs module is been used. Each api route is been tested, without mocking the mongo calls. Thats why we need a container with mongo on containers test.

### Running all tests

To run all tests, there are test containers, so you just need to type:

```javascript
$ sudo make tests
```