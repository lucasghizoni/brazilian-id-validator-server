'use strict';

const mongoose = require('mongoose'),
    validateCompany = require('../validator/IdValidator').validateCompany,
    Company = mongoose.model('Company');

exports.list = (req, res) => {
    Company.find({}, (err, companies) => {
        if (err) {
            return res.status(500).send(err);
        }
        return res.json(companies);
    });
};

exports.create = (req, res) => {
    if (!validateCompany(req.body.id)) {
        return res.status(400).send({ message: "Invalid ID" });
    }

    const company = new Company({
        _id: req.body.id,
        blacklist: req.body.blacklist
    });

    company.save(function (err, companySaved) {
        if (err && err.code === 11000) {
            //303 HTTP code for conflit of an existing resource. 
            //See more at: https://tools.ietf.org/html/rfc7231#section-4.3.3
            return res.status(303).send({ message: "Company already exists" });
        }
        if (err) {
            return res.status(500).send(err);
        }
        return res.json(companySaved);
    });
};

exports.edit = (req, res) => {
    if (!validateCompany(req.body.id)) {
        return res.status(400).send({ message: "Invalid ID" });
    }

    Company.remove({ _id: req.params.id }, (err, commandResult) => {
        if (err) {
            return res.send(err);
        }
        if (commandResult.result.n === 0) {
            return res.status(404).send({ message: "Company not found" });
        }

        const company = new Company({
            _id: req.body.id,
            blacklist: req.body.blacklist
        });
    
        company.save(function (err, companySaved) {
            if (err && err.code === 11000) {
                return res.status(303).send({ message: "Company already exists" });
            }
            if (err) {
                return res.status(500).send(err);
            }
            return res.json(companySaved);
        }); 
    });
};

exports.read = (req, res) => {
    if (!validateCompany(req.params.id)) {
        return res.status(400).send({ message: "Invalid ID" });
    }
    
    Company.findOne({ _id: req.params.id }, (err, company) => {
        if (err) {
            return res.status(500).send(err);
        }
        if (!company) {
            return res.status(404).send({ message: "Company not found" });
        }
        return res.json(company);
    });
};

exports.delete = (req, res) => {
    Company.remove({ _id: req.params.id }, (err, commandResult) => {
        if (err) {
            return res.send(err);
        }
        if (commandResult.result.n === 0) {
            return res.status(404).send({ message: "Company not found" });
        }
        return res.json({ message: 'Company successfully deleted' });
    });
};