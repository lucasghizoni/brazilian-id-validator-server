'use strict';

const mongoose = require('mongoose'),
    validateIndividual = require('../validator/IdValidator').validateIndividual,
    Individual = mongoose.model('Individual');

exports.list = (req, res) => {
    Individual.find({}, (err, individuals) => {
        if (err) {
            return res.status(500).send(err);
        }
        return res.json(individuals);
    });
};

exports.create = (req, res) => {
    if (!validateIndividual(req.body.id)) {
        return res.status(400).send({ message: "Invalid ID" });
    }

    const individual = new Individual({
        _id: req.body.id,
        blacklist: req.body.blacklist
    });

    individual.save(function (err, individualSaved) {
        if (err && err.code === 11000) {
            //303 HTTP code for conflit of an existing resource. 
            //See more at: https://tools.ietf.org/html/rfc7231#section-4.3.3
            return res.status(303).send({ message: "Individual already exists" });
        }
        if (err) {
            return res.status(500).send(err);
        }
        return res.json(individualSaved);
    });
};

exports.edit = (req, res) => {
    if (!validateIndividual(req.body.id)) {
        return res.status(400).send({ message: "Invalid ID" });
    }

    Individual.remove({ _id: req.params.id }, (err, commandResult) => {
        if (err) {
            return res.send(err);
        }
        if (commandResult.result.n === 0) {
            return res.status(404).send({ message: "Individual not found" });
        }

        const individual = new Individual({
            _id: req.body.id,
            blacklist: req.body.blacklist
        });
    
        individual.save(function (err, individualSaved) {
            if (err && err.code === 11000) {
                return res.status(303).send({ message: "Individual already exists" });
            }
            if (err) {
                return res.status(500).send(err);
            }
            return res.json(individualSaved);
        }); 
    });
};

exports.read = (req, res) => {
    if (!validateIndividual(req.params.id)) {
        return res.status(400).send({ message: "Invalid ID" });
    }
    
    Individual.findOne({ _id: req.params.id }, (err, individual) => {
        if (err) {
            return res.status(500).send(err);
        }
        if (!individual) {
            return res.status(404).send({ message: "Individual not found" });
        }
        return res.json(individual);
    });
};

exports.delete = (req, res) => {
    Individual.remove({ _id: req.params.id }, (err, commandResult) => {
        if (err) {
            return res.send(err);
        }
        if (commandResult.result.n === 0) {
            return res.status(404).send({ message: "Individual not found" });
        }
        return res.json({ message: 'Individual successfully deleted' });
    });
};