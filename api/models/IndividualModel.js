'use strict';

const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const IndividualSchema = new Schema({
    _id: {
        type: String,
        required: 'id is required'
    },
    blacklist: {
        type: Boolean,
        default: false
    }
});

IndividualSchema.set('toJSON', {
    virtuals: true,
    versionKey:false,
    transform: function (doc, ret) {
        ret.id = ret._id;
        delete ret._id;
    }
  });

module.exports = mongoose.model('Individual', IndividualSchema, 'individuals');