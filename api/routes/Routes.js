'use strict';

module.exports = app => {
    const individualController = require('../controllers/IndividualController'),
        companyController = require('../controllers/CompanyController');

    app.route('/individuals')
        .get(individualController.list)
        .post(individualController.create);

    app.route('/individual/:id')
        .get(individualController.read)
        .put(individualController.edit)
        .delete(individualController.delete);

        
    app.route('/companies')
        .get(companyController.list)
        .post(companyController.create);

    app.route('/company/:id')
        .get(companyController.read)
        .put(companyController.edit)
        .delete(companyController.delete);
};