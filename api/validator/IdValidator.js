exports.validateIndividual = id => {

    if (typeof id !== "string" || isNaN(Number(id)) || id.length !== 11) {
        return false;
    }

    const firstVerifierDigit = Number(id[9]),
        secondVerifierDigit = Number(id[10]);

    let idNumbers = [...id.substring(0, 9)].map(num => Number(num)),
        multipliers = [10, 9, 8, 7, 6, 5, 4, 3, 2];

    const firstDigitVerifierResult = calculateDigit(idNumbers, multipliers);

    idNumbers.push(firstDigitVerifierResult);
    multipliers = [11, ...multipliers];

    const secondDigitVerifierResult = calculateDigit(idNumbers, multipliers);
    idNumbers.push(secondDigitVerifierResult);

    return id === idNumbers.join('');
}

exports.validateCompany = id => {
    if (typeof id !== "string" || isNaN(Number(id)) || id.length !== 14) {
        return false;
    }

    const firstVerifierDigit = Number(id[12]),
        secondVerifierDigit = Number(id[13]);

    let idNumbers = [...id.substring(0, 12)].map(num => Number(num)),
        multipliers = [5, 4, 3, 2, 9, 8, 7, 6, 5, 4, 3, 2];

    const firstDigitVerifierResult = calculateDigit(idNumbers, multipliers);

    idNumbers.push(firstDigitVerifierResult);
    multipliers = [6, ...multipliers];

    const secondDigitVerifierResult = calculateDigit(idNumbers, multipliers);
    idNumbers.push(secondDigitVerifierResult);

    return id === idNumbers.join('');
}

const calculateDigit = (idNumbers, multipliers) => {
    let multiplicationResult = 0;

    idNumbers.forEach((num, index) => {
        multiplicationResult += num * multipliers[index];
    });

    const divisionRest = multiplicationResult % 11;

    return divisionRest < 2 ? 0 : 11 - divisionRest;
}