const express = require('express'),
    app = express(),
    port = 3000,
    mongoose = require('mongoose'),
    Individual = require('./api/models/IndividualModel'),
    CompanyModel = require('./api/models/CompanyModel'),
    bodyParser = require('body-parser');

const dbName = process.env.NODE_ENV === 'test' ? "Idstestdb" : "Idsdb";
mongoose.connect('mongodb://mongo:27017/' + dbName);

let serverInfo = {
    requests: 0,
    uptime: ''
};

//middleware to allow cross origin domain
const allowCrossDomain = (req, res, next) => {
    serverInfo.requests++;
    res.header('Access-Control-Allow-Origin', '*');
    res.header('Access-Control-Allow-Methods', 'GET,POST,DELETE,PUT');
    res.header('Access-Control-Allow-Headers', 'Content-Type');
    next();
};

app.use(allowCrossDomain);
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

var routes = require('./api/routes/Routes');
routes(app);

app.route('/status').get( (req, res ) => {
    serverInfo.requests--; // we are not counting requests to /status;
    const minutes = parseInt(process.uptime() / 60);
    serverInfo.uptime = minutes > 1 ? minutes + ' minutes' : "Just started now";
    return res.send(serverInfo);
});

if(process.env.NODE_ENV !== 'test'){
    app.listen(port);
    console.log('Server started on: ' + port);
}

module.exports = app; // this is for API integration tests only