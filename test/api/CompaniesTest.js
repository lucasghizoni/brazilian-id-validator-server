const mongoose = require("mongoose");

require('../../api/models/CompanyModel');

const Company = mongoose.model('Company');

const chai = require('chai'),
    assert = require('assert'),
    chaiHttp = require('chai-http'),
    server = require('../../server'),
    should = chai.should();

chai.use(chaiHttp);

describe('Routes of company model', function () {

    beforeEach(function (done) {
        Company.remove({}, (err) => {
            done();
        });
    });

    describe('/companies route', function () {
        it('it should GET all companies', function (done) {
            chai.request(server)
                .get('/companies')
                .end(function (err, res) {
                    res.should.have.status(200);
                    res.body.should.be.a('array');
                    res.body.length.should.be.eql(0);
                    done();
                });
        });
    
        it('it should return message invalid ID when tring to POST an invalid ID', function (done) {
            const company = {
                id: "123123n2131n3",
                blacklist: true
            };
    
            chai.request(server)
                .post('/companies')
                .send(company)
                .end(function (err, res) {
                    res.should.have.status(400);
                    res.body.should.be.an('object');
                    res.body.should.have.property('message');
                    res.body.should.have.property('message').eql('Invalid ID');
                    done();
                });
        });
        
        it('it should return 200 when posting a valid company', function (done) {
            const company = {
                id: "71436687000150",
                blacklist: false
            };
    
            chai.request(server)
                .post('/companies')
                .send(company)
                .end(function (err, res) {
                    res.should.have.status(200);
                    res.body.should.be.an('object');
                    res.body.should.have.property('id').eql(company.id);
                    res.body.should.have.property('blacklist').eql(company.blacklist);
                    done();
                });
        });
    
        it('it should return HTTP 303 when tring to insert an ID that is already saved on database', function (done) {
            const company = new Company({
                _id: "71436687000150",
                blacklist: true
            });
    
            company.save(function (err, companySaved) {
                chai.request(server)
                    .post('/companies')
                    .send({
                        id: "71436687000150",
                        blacklist: true
                    })
                    .end(function (err, res) {
                        assert.equal(res.statusCode, 303);
                        done();
                    });
            });
        });
    });

    describe('/company/:id route', function () {
        it('it should return the correct company when requesting with GET', function (done) {
            const company = new Company({
                _id: "71436687000150",
                blacklist: true
            });
    
            company.save(function (err, companySaved) {
                chai.request(server)
                    .get('/company/71436687000150')
                    .end(function (err, res) {
                        res.should.have.status(200);
                        res.body.should.be.an('object');
                        res.body.should.have.property('id').eql("71436687000150");
                        res.body.should.have.property('blacklist').eql(true);
                        done();
                    });
            });
        });

        it('it should return 404 when trying to GET a company that is not on database', function (done) {
            chai.request(server)
                .get('/company/50314873000147')
                .end(function (err, res) {
                    res.should.have.status(404);
                    res.body.should.be.an('object');
                    res.body.should.have.property('message').eql("Company not found");
                    done();
                });
        });

        it('it should DELETE a company on database', function (done) {
            const company = new Company({
                _id: "71436687000150",
                blacklist: true
            });
    
            company.save(function (err, companySaved) {
                chai.request(server)
                    .delete('/company/71436687000150')
                    .end(function (err, res) {
                        res.should.have.status(200);
                        res.body.should.be.an('object');
                        res.body.should.have.property('message').eql("Company successfully deleted");
                        done();
                    });
            });
        });

        it('it should return 404 when trying to DELETE a company that is not on database', function (done) {
            chai.request(server)
                .get('/company/71436687000150')
                .end(function (err, res) {
                    res.should.have.status(404);
                    res.body.should.be.an('object');
                    res.body.should.have.property('message').eql("Company not found");
                    done();
                });
        });
    });

});