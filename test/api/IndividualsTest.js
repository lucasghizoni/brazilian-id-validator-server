const mongoose = require("mongoose");

require('../../api/models/IndividualModel');

const Individual = mongoose.model('Individual');

const chai = require('chai'),
    assert = require('assert'),
    chaiHttp = require('chai-http'),
    server = require('../../server'),
    should = chai.should();

chai.use(chaiHttp);

describe('Routes of individual model', function () {

    beforeEach(function (done) {
        Individual.remove({}, (err) => {
            done();
        });
    });

    describe('/individuals route', function () {
        it('it should GET all individuals', function (done) {
            chai.request(server)
                .get('/individuals')
                .end(function (err, res) {
                    res.should.have.status(200);
                    res.body.should.be.a('array');
                    res.body.length.should.be.eql(0);
                    done();
                });
        });
    
        it('it should return message invalid ID when tring to POST an invalid ID', function (done) {
            const individual = {
                id: "123123n2131n3",
                blacklist: true
            };
    
            chai.request(server)
                .post('/individuals')
                .send(individual)
                .end(function (err, res) {
                    res.should.have.status(400);
                    res.body.should.be.an('object');
                    res.body.should.have.property('message');
                    res.body.should.have.property('message').eql('Invalid ID');
                    done();
                });
        });
        
        it('it should return 200 when posting a valid individual', function (done) {
            const individual = {
                id: "06875446960",
                blacklist: true
            };
    
            chai.request(server)
                .post('/individuals')
                .send(individual)
                .end(function (err, res) {
                    res.should.have.status(200);
                    res.body.should.be.an('object');
                    res.body.should.have.property('id').eql(individual.id);
                    res.body.should.have.property('blacklist').eql(individual.blacklist);
                    done();
                });
        });
    
        it('it should return HTTP 303 when tring to insert an ID that is already saved on database', function (done) {
            const individual = new Individual({
                _id: "06875446960",
                blacklist: true
            });
    
            individual.save(function (err, individualSaved) {
                chai.request(server)
                    .post('/individuals')
                    .send({
                        id: "06875446960",
                        blacklist: true
                    })
                    .end(function (err, res) {
                        assert.equal(res.statusCode, 303);
                        done();
                    });
            });
        });
    });

    describe('/individual/:id route', function () {
        it('it should return the correct individual when requesting with GET', function (done) {
            const individual = new Individual({
                _id: "06875446960",
                blacklist: true
            });
    
            individual.save(function (err, individualSaved) {
                chai.request(server)
                    .get('/individual/06875446960')
                    .end(function (err, res) {
                        res.should.have.status(200);
                        res.body.should.be.an('object');
                        res.body.should.have.property('id').eql(individual.id);
                        res.body.should.have.property('blacklist').eql(individual.blacklist);
                        done();
                    });
            });
        });

        it('it should return 404 when trying to GET an individual that is not on database', function (done) {
            chai.request(server)
                .get('/individual/06875446960')
                .end(function (err, res) {
                    res.should.have.status(404);
                    res.body.should.be.an('object');
                    res.body.should.have.property('message').eql("Individual not found");
                    done();
                });
        });

        it('it should DELETE an individual on database', function (done) {
            const individual = new Individual({
                _id: "06875446960",
                blacklist: true
            });
    
            individual.save(function (err, individualSaved) {
                chai.request(server)
                    .delete('/individual/06875446960')
                    .end(function (err, res) {
                        res.should.have.status(200);
                        res.body.should.be.an('object');
                        res.body.should.have.property('message').eql("Individual successfully deleted");
                        done();
                    });
            });
        });

        it('it should return 404 when trying to DELETE an individual that is not on database', function (done) {
            chai.request(server)
                .get('/individual/06875446960')
                .end(function (err, res) {
                    res.should.have.status(404);
                    res.body.should.be.an('object');
                    res.body.should.have.property('message').eql("Individual not found");
                    done();
                });
        });
    });

});