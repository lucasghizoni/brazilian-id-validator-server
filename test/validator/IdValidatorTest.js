const assert = require('assert'),
    idValidator = require('../../api/validator/IdValidator');

describe('IdValidator unit tests', function () {
    describe('#validateCpf()', function () {
        it('Return false when other type than String is passed', function () {
            const ret = idValidator.validateIndividual(12312312332);
            assert.equal(ret, false);
        });

        it('Return false when string not contains only numbers', function () {
            const ret = idValidator.validateIndividual("123n3312332");
            assert.equal(ret, false);
        });

        it('Return false when string not contains length 11', function () {
            const ret = idValidator.validateIndividual("123312332");
            assert.equal(ret, false);
        });

        it('Return true when a valid CPF is passed', function () {
            const ret = idValidator.validateIndividual("06875446960");
            assert.equal(ret, true);
        });

        it('Return false when an invalid CPF is passed', function () {
            const ret = idValidator.validateIndividual("06875446961");
            assert.equal(ret, false);
        });
    });

    describe('#validateCompany()', function () {
        it('Return false when other type than String is passed', function () {
            const ret = idValidator.validateCompany(11344424000190);
            assert.equal(ret, false);
        });

        it('Return false when string not contains only numbers', function () {
            const ret = idValidator.validateCompany("113n4424000190");
            assert.equal(ret, false);
        });

        it('Return false when string not contains length 11', function () {
            const ret = idValidator.validateCompany("1134442400019");
            assert.equal(ret, false);
        });

        it('Return true when a valid CNPJ is passed', function () {
            const ret = idValidator.validateCompany("11344424000190");
            assert.equal(ret, true);
        });

        it('Return false when an invalid CNPJ is passed', function () {
            const ret = idValidator.validateCompany("11344424000191");
            assert.equal(ret, false);
        });
    });
});